//Student Name: Ajaydeep Singh
//ID:C0744219
//  ScreamItus_IOSUITests.swift
//  ScreamItus-IOSUITests
//
//  Created by AJAY BAJWA on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

class ScreamItus_IOSUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTextBoxButtonVisible() {
        
        let app = XCUIApplication()
        //Getting the text field
        let textField = app.otherElements.containing(.staticText, identifier:"Infection Calculator")
        //Checking if txet field exists
        XCTAssertNotNil(textField)
        //Getting the calculate Button
        let calculateButton = app.buttons["Calculate"]
        //check if the button exists
        XCTAssertNotNil(calculateButton)
        
        let resultsLabel = app.staticTexts["1000 instructors infected!"]
        XCTAssertTrue(resultsLabel.isEnabled)
    }
    
    func testCorrectInfectedInstructors() {
        
        let app = XCUIApplication()
        //Getting the Text Field from UI
        let textField = app.otherElements.containing(.staticText, identifier:"Infection Calculator").children(matching: .textField)
        //Tapping the text field
        textField.element.tap()
        textField.setText("9", forKeyPath: app)
        //Getting the Calculate button
        let calButton = app.buttons["Calculate"]
        //Tapping the calculate button
        calButton.tap()
        //Getting the result label showing the no. of instructors infected
        let resultLabel = app.staticTexts["62 instructors infected"]
        //Checking if the results label is not null
        XCTAssertNotNil(resultLabel)
        
    }
    

}

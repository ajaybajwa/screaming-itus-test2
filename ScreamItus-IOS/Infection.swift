//Student Name: Ajaydeep Singh
//ID: C0744219
//  Infection.swift
//  ScreamItus-IOS
//
//  Created by parrot on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import Foundation
class Infection {
    var totalNumberInfected = 0
    var infectionRate = 1
    
    func calculateTotalInfected(day: Int) -> Int{
        //R1. The number of days must be more/equal to 1
        if (day<=0){
            return -1
        }
            //R2. Virus infects instructors at rate of 5 per day
        else if (day>=1 && day<=7){
            if(( day % 2) == 0){
                infectionRate = 0
            }
            else{
                infectionRate = 5
            }
            totalNumberInfected = day*infectionRate
        }
            //R3. After 7 days, infection rate changes to 8 per day
        else if(day>7){
            var n = 3;
            for _ in 8...day{
                n = n-1
            }
            if(( day % 2) == 0){
                infectionRate = 0
                totalNumberInfected = day*infectionRate
            }
            else{
                infectionRate = 8
                totalNumberInfected = (day*infectionRate) - (day+n)
            }
            
        }
            //R4. if day number is even infection rate drops to 0
        return totalNumberInfected
    }
    
}

//Student Name: Ajaydeep Singh
//ID: C0744219
//  ScreamItus_IOSTests.swift
//  ScreamItus-IOSTests
//
//  Created by AJAY BAJWA on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest
@testable import ScreamItus_IOS
class ScreamItus_IOSTests: XCTestCase {

    var infection: Infection!
    override func setUp() {
        super.setUp()
        // 3. Initialize the global variable
        // -  JAVA: game = new BullEyeGame();
        infection = Infection();
    }

    override func tearDown() {
        infection = nil
        super.tearDown()
    }

    func testNumberOfDaysMoreThan0() {
        //R1 If no. of days is less than 1 it returns -1
        let days = 0
        let daysStatus = infection.calculateTotalInfected(day: days)
        XCTAssertEqual(-1, daysStatus)
    }

    func testTotalInfectedLessThan8() {
        // R2.if days<=7, infection rate is 5
        let days = 5
        let expectedTotalinfected = 25
        let actualTotal = infection.calculateTotalInfected(day: days)
        XCTAssertEqual(expectedTotalinfected, actualTotal)
    }
    func testTotalInfectedMoreThan7() {
        //R3. if days<=7, infection rate is 5
        let days = 9
        let expectedTotalinfected = 62
        let actualTotal = infection.calculateTotalInfected(day: days)
        XCTAssertEqual(expectedTotalinfected, actualTotal)
    }
    func testTotalInfectedEvenDays() {
        //R4. if days<=7, infection rate is 5
        let days = 10
        let expectedTotalinfected = 0
        let actualTotal = infection.calculateTotalInfected(day: days)
        XCTAssertEqual(expectedTotalinfected, actualTotal)
    }
    
    

}
